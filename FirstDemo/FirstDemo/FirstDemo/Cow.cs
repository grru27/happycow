﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;
using System.Text.RegularExpressions;

namespace FirstDemo
{
    public enum CowAction { Eating, Muuuh, Stopped, Walking };

    public enum CowDirection { East, North, South, West, NorthEast, NorthWest, SouthEast, SouthWest };
         
    public class Cow 
    {
        public CowAction Action { get; set; }
        public CowDirection Direction { get; set; }
        public Vector2 Position { get; set; }

        Dictionary<CowAction, Dictionary<CowDirection, List<Texture2D>>> textures = new Dictionary<CowAction, Dictionary<CowDirection, List<Texture2D>>>();
        DateTime lastRotationTime;
        const int rotationTimeout = 100;
        DateTime lastAnimationTime;
        const int animationTimeout = 100;
        int animationIndex;

        public Cow(float x, float y, CowAction action, CowDirection direction, ContentManager contentManager)
        {
            Action = action;
            Direction = direction;
            Position = new Vector2(x, y);

            foreach (CowAction a in Enum.GetValues(typeof(CowAction)))
            {
                textures.Add(a, new Dictionary<CowDirection, List<Texture2D>>());
                foreach (CowDirection d in Enum.GetValues(typeof(CowDirection)))
                {
                    textures[a].Add(d, new List<Texture2D>());
                }
            }

            LoadContent(contentManager);

            DateTime now = DateTime.Now;
            lastRotationTime = now;
            lastAnimationTime = now;
            animationIndex = 0;
        }

        void LoadContent(ContentManager contentManager)
        {
            foreach (CowAction action in Enum.GetValues(typeof(CowAction)))
            {
                foreach (CowDirection direction in Enum.GetValues(typeof(CowDirection)))
                {
                    bool done = false;
                    int i = 0;
                    string dir = Regex.Replace(direction.ToString(), "[a-z]", "");

                    while (!done)
                    {
                        string name = string.Format(@"Cow\{0} {1}{2:0000}",
                            action.ToString().ToLower(),
                            dir.ToString().ToLower(),
                            i);

                        try
                        {
                            textures[action][direction].Add(contentManager.Load<Texture2D>(name));
                        }
                        catch (Exception ex)
                        {
                            done = true;
                        }
                        i++;
                    }
                }
            }
        }

        public void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {            
            DateTime now = DateTime.Now;

            if ((now - lastAnimationTime).TotalMilliseconds > animationTimeout)
            {
                lastAnimationTime = now;
                animationIndex++;
                if (animationIndex >= textures[Action][Direction].Count)
                {
                    animationIndex = 0;
                }
            }
            spriteBatch.Draw(textures[Action][Direction][animationIndex], Position, Color.White);            
        }

        public void RotateLeft()
        {
            DateTime now = DateTime.Now;

            if ((now - lastRotationTime).TotalMilliseconds > rotationTimeout)
            {
                lastRotationTime = now;

                switch (Direction)
                {
                    case CowDirection.North:
                        Direction = CowDirection.NorthWest;
                        break;
                    case CowDirection.NorthEast:
                        Direction = CowDirection.North;
                        break;
                    case CowDirection.East:
                        Direction = CowDirection.NorthEast;
                        break;
                    case CowDirection.SouthEast:
                        Direction = CowDirection.East;
                        break;
                    case CowDirection.South:
                        Direction = CowDirection.SouthEast;
                        break;
                    case CowDirection.SouthWest:
                        Direction = CowDirection.South;
                        break;
                    case CowDirection.West:
                        Direction = CowDirection.SouthWest;
                        break;
                    case CowDirection.NorthWest:
                        Direction = CowDirection.West;
                        break;
                }
            }
        }

        public void RotateRight()
        {
            DateTime now = DateTime.Now;            

            if ((now - lastRotationTime).TotalMilliseconds > rotationTimeout)
            {
                lastRotationTime = now;

                switch (Direction)
                {
                    case CowDirection.North:
                        Direction = CowDirection.NorthEast;
                        break;
                    case CowDirection.NorthEast:
                        Direction = CowDirection.East;
                        break;
                    case CowDirection.East:
                        Direction = CowDirection.SouthEast;
                        break;
                    case CowDirection.SouthEast:
                        Direction = CowDirection.South;
                        break;
                    case CowDirection.South:
                        Direction = CowDirection.SouthWest;
                        break;
                    case CowDirection.SouthWest:
                        Direction = CowDirection.West;
                        break;
                    case CowDirection.West:
                        Direction = CowDirection.NorthWest;
                        break;
                    case CowDirection.NorthWest:
                        Direction = CowDirection.North;
                        break;
                }
            }
        }

        public bool ContainsPoint(Vector2 point)
        {
            bool result = false;

            if ( (point.X >= Position.X) 
              && (point.X <= (Position.X + textures[Action][Direction][animationIndex].Width)) 
              && (point.Y >= Position.Y)
              && (point.Y <= (Position.Y + textures[Action][Direction][animationIndex].Height))
                )
            {
                result = true;
            }

            return result;
        }
    }
}
