using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace FirstDemo
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Main : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        Cow cow0;
        Cow cow1;
        Cow cow2;
        Cow cow3;

        SpriteFont Font1;
        string cowName = string.Empty;
        Vector2 FontPos;

        public Main()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            cow0 = new Cow(100f, 100f, CowAction.Stopped, CowDirection.East, this.Content);
            cow1 = new Cow(200f, 100f, CowAction.Eating, CowDirection.East, this.Content);
            cow2 = new Cow(100f, 200f, CowAction.Muuuh, CowDirection.East, this.Content);
            cow3 = new Cow(200f, 200f, CowAction.Walking, CowDirection.East, this.Content);

            Font1 = Content.Load<SpriteFont>("Font1");
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            // TODO: Add your update logic here

            if (Keyboard.GetState().IsKeyDown(Keys.Right) && !Keyboard.GetState().IsKeyDown(Keys.Left))
            {
                cow0.RotateRight();
                cow1.RotateRight();
                cow2.RotateRight();
                cow3.RotateRight();
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Left) && !Keyboard.GetState().IsKeyDown(Keys.Right))
            {
                cow0.RotateLeft();
                cow1.RotateLeft();
                cow2.RotateLeft();
                cow3.RotateLeft();
            }

            MouseState current_mouse = Mouse.GetState();
            FontPos = new Vector2(current_mouse.X, current_mouse.Y);
            if (cow0.ContainsPoint(FontPos))
            {
                cowName = "cow0";
            }
            else if (cow1.ContainsPoint(FontPos))
            {
                cowName = "cow1";
            }
            else if (cow2.ContainsPoint(FontPos))
            {
                cowName = "cow2";
            }
            else if (cow3.ContainsPoint(FontPos))
            {
                cowName = "cow3";
            }
            else
            {
                cowName = string.Empty;
            }

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here
            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend);

            cow0.Draw(spriteBatch, gameTime);
            cow1.Draw(spriteBatch, gameTime);
            cow2.Draw(spriteBatch, gameTime);
            cow3.Draw(spriteBatch, gameTime);

            if (!string.IsNullOrEmpty(cowName))
            {
                spriteBatch.DrawString(Font1, cowName, FontPos, Color.White);
            }

            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
